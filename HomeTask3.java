import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.events.WebDriverEventListener;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.beans.EventHandler;


public class HomeTask3 {
    public static void main(String[] args){
        WebDriver driver = inChrome();

        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
        WebElement email = driver.findElement(By.id("email"));
        WebElement pass = driver.findElement(By.id("passwd"));
        WebElement btnin = driver.findElement(By.name("submitLogin"));
        email.sendKeys("webinar.test@gmail.com");
        pass.sendKeys("Xcg7299bnSmMuRLp9ITw");
        btnin.click();
        WebDriverWait wait = (WebDriverWait) new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("menu")));
        WebElement catalog = driver.findElement(By.id("subtab-AdminCatalog"));
        Actions catmove = new Actions(driver);
        catmove.moveToElement(catalog).perform();
        WebElement contacts = driver.findElement(By.xpath("//*[@id=\"subtab-AdminCategories\"]"));
        WebDriverWait wait1 = (WebDriverWait) new WebDriverWait(driver, 10);
        wait1.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"subtab-AdminCategories\"]")));
        contacts.click();

        WebDriverWait wait2 = new WebDriverWait(driver, 10);
        wait2.until(ExpectedConditions.presenceOfElementLocated(By.id("page-header-desc-category-new_category")));
        WebElement addcat = driver.findElement(By.id("page-header-desc-category-new_category"));
        addcat.click();
        WebDriverWait wait3 = new WebDriverWait(driver, 10);
        wait3.until(ExpectedConditions.presenceOfElementLocated(By.id("name_1")));
        WebElement catname = driver.findElement(By.id("name_1"));

        Integer cnum = (int)(Math.random()*10);
        String a = "TestCategory"+cnum.toString();
        catname.sendKeys(a);
        WebElement savebtn = driver.findElement(By.id("category_form_submit_btn"));
        savebtn.click();

        String success = "Создано";
        String allert = driver.findElement(By.xpath(".//*[contains(text(), success)]")).getText();

        WebElement filtername = driver.findElement(By.xpath("//*[@id=\"table-category\"]/thead/tr[1]/th[3]/span/a[1]/i"));
        filtername.click();
        driver.findElement(By.xpath(".//*[contains(text(), a)]"));



    }
    public static WebDriver inChrome(){
        System.setProperty("webdriver.chrome.driver", HomeTask2.class.getResource("chromedriver.exe").getPath());
        return new ChromeDriver();
    }
}
