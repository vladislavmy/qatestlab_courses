import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;

import java.text.DecimalFormat;

public class HomeTask4 {
    private WebDriver driver;

    @BeforeClass
    public void beforeClass() {
        System.setProperty("webdriver.chrome.driver", HomeTask4.class.getResource("chromedriver.exe").getPath());
    }
    @DataProvider(name = "Creds")
    public static Object[][] getCreds() {
        return new Object[][]{{"webinar.test@gmail.com","Xcg7299bnSmMuRLp9ITw"}};
    }
    @BeforeMethod()
    public void setUp() {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
        ChromeOptions options = new ChromeOptions();
        options.addArguments("incognito");
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
        driver.get("http://prestashop-automation.qatestlab.com.ua/admin147ajyvk0/");
    }


    @AfterMethod
    public void tearDown() {
        driver.quit();
    }
    Integer num1 = (int)(Math.random()*50);
    String item_name = "TestItem"+num1.toString();
    @Test(dataProvider = "Creds")
    public void addNewItem(String user, String password){
        WebDriverWait wait = (WebDriverWait) new WebDriverWait(driver, 10);

        WebElement email = driver.findElement(By.id("email"));
        WebElement pass = driver.findElement(By.id("passwd"));
        WebElement btnin = driver.findElement(By.name("submitLogin"));
        email.sendKeys(user);
        pass.sendKeys(password);
        btnin.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("menu")));
        WebElement catalog = driver.findElement(By.id("subtab-AdminCatalog"));
        catalog.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[contains(text(), 'Новый товар')]")));
        WebElement add_button = driver.findElement(By.xpath("//span[contains(text(), 'Новый товар')]"));
        add_button.click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("form_step1_names")));


        Integer num2 = (int)(Math.random()*100);
        String item_q = num2.toString();

        Double num3 = Math.random()*99.9+0.1;

        String pattern = "##0.0";
        DecimalFormat decimalFormat = new DecimalFormat(pattern);

        String format_p = decimalFormat.format(num3);
        System.out.println(format_p);

        WebElement name = driver.findElement(By.xpath("//*[@id=\"form_step1_name_1\"]"));
        name.sendKeys(item_name);

        WebElement quantity = driver.findElement(By.id("form_step1_qty_0_shortcut"));
        quantity.clear();
        quantity.sendKeys(item_q);

        WebElement price = driver.findElement(By.id("form_step1_price_shortcut"));
        price.clear();
        price.sendKeys(format_p);

        WebElement switcher = driver.findElement(By.className("switch-input"));
        switcher.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(), 'Настройки обновлены')]")));
        WebElement set_close = driver.findElement(By.xpath("//div[contains(@class, 'growl-close')]"));
        set_close.click();


        WebElement save_btn = driver.findElement(By.xpath("//input[contains(@id, 'submit')]"));
        save_btn.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[contains(text(), 'Настройки обновлены')]")));
        WebElement set_close1 = driver.findElement(By.xpath("//div[contains(@class, 'growl-close')]"));
        set_close1.click();
    }

    @Test
    public void checkItem(){
        WebDriverWait wait = (WebDriverWait) new WebDriverWait(driver, 10);
        driver.get("http://prestashop-automation.qatestlab.com.ua/ru/");

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[contains(@class, 'all-product-link pull-xs-left pull-md-right h4')]")));
        WebElement all_items = driver.findElement(By.xpath("//a[contains(@class, 'all-product-link pull-xs-left pull-md-right h4')]"));
        all_items.click();

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("products")));

        String items_present = driver.findElement(By.id("products")).getText();

        Assert.assertTrue(items_present.contains(item_name));

    }
}